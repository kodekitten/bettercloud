package net.kodekitten.BetterCloud;

import net.kodekitten.BetterCloud.processor.ProcessorFactory;
import net.kodekitten.BetterCloud.processor.ProcessorType;
import net.kodekitten.BetterCloud.service.ContactService;

import java.util.List;

/**
 * Hello world!
 */
public class App {


    public static void main(String[] args) {


        List contacts = ProcessorFactory.getProcessor(ProcessorType.CSV).process(args[0]);


        ContactService.batchAdd(contacts);


    }

}
