package net.kodekitten.BetterCloud.model;

/**
 * Created by kodekitten on 8/20/14.
 */
public class Contact {
    private String fullName;
    private String email;
    private String phone;

    public Contact() {
    }

    public Contact(String fullName, String email, String phone) {
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
