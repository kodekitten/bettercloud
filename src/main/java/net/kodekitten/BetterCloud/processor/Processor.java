package net.kodekitten.BetterCloud.processor;

import java.util.List;

/**
 * Created by kodekitten on 8/20/14.
 */
public interface Processor {

    public List process(String filename);
}
