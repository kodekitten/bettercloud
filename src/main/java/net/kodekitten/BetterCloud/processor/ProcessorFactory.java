package net.kodekitten.BetterCloud.processor;

import net.kodekitten.BetterCloud.processor.csv.CSVProcessor;

/**
 * Created by kodekitten on 8/20/14.
 */
public class ProcessorFactory {
    private static CSVProcessor csvProcessor = new CSVProcessor();


    public static Processor getProcessor(ProcessorType type) {
        switch (type) {
            case CSV:
                return csvProcessor;
        }
        return null;
    }
}
