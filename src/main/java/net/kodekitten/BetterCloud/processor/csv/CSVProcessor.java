package net.kodekitten.BetterCloud.processor.csv;

import net.kodekitten.BetterCloud.model.Contact;
import net.kodekitten.BetterCloud.processor.Processor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kodekitten on 8/20/14.
 */

public class CSVProcessor implements Processor {
    @Override
    public List process(String filename) {
        BufferedReader reader = null;
        List<Contact> contacts = new ArrayList<Contact>();
        try {
            reader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            System.out.println("Unable to find " + filename);
        }

        System.out.println("" + new Date() + " Starting processing of file: " + filename);

        CsvReader csvReader = new CsvReader(reader);

        try {
            csvReader.readRecord();
            while (csvReader.readRecord()) {
                contacts.add(new Contact(csvReader.get(0), csvReader.get(1), csvReader.get(2)));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        csvReader.close();
        System.out.println("" + new Date() + " Records Processed: " + contacts.size());


        return contacts;
    }
}
