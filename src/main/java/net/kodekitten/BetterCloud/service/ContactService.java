package net.kodekitten.BetterCloud.service;

import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.batch.BatchOperationType;
import com.google.gdata.data.batch.BatchUtils;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.extensions.Email;
import com.google.gdata.data.extensions.FullName;
import com.google.gdata.data.extensions.Name;
import com.google.gdata.data.extensions.PhoneNumber;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import net.kodekitten.BetterCloud.model.Contact;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;

/**
 * Created by kodekitten on 8/20/14.
 */
public class ContactService {
    private static int MAX_BATCH = 25;
    private static String USERNAME = "test.exercise5@test.cloud8labs.com";
    private static String PASSWORD = "abc123abc";
    private ContactsService service;

    private static ContactService contactService = new ContactService();

    public static void batchAdd(List<Contact> contactList) {
        contactService.service = new com.google.gdata.client.contacts.ContactsService("BetterCloud upload");


        try {
            contactService.service.setUserCredentials(USERNAME, PASSWORD);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }

        ContactFeed contactFeed = new ContactFeed();
        Date batchStart = new Date();

        System.out.println(batchStart + " Batch started");
        for (int i = 0; i < contactList.size(); i++) {
            ContactEntry contactEntry = contactService.createContactEntry(contactList.get(i));
            BatchUtils.setBatchId(contactEntry, "create " + i);
            BatchUtils.setBatchOperationType(contactEntry, BatchOperationType.INSERT);
            contactFeed.getEntries().add(contactEntry);


            if (i % MAX_BATCH == 0) {
                contactService.sendBatch(contactFeed);
                contactFeed = new ContactFeed();
            }

        }

        Date batchEnd = new Date();

        long completedTime = batchEnd.getTime() - batchStart.getTime();
        System.out.println(batchEnd + " Batch completed in " + completedTime + "ms");
    }

    private ContactEntry createContactEntry(Contact contact) {
        ContactEntry createContact = new ContactEntry();
        final String NO_YOMI = null;
        Name name = new Name();
        name.setFullName(new FullName(contact.getFullName(), NO_YOMI));
        createContact.setName(name);

        Email primaryMail = new Email();
        primaryMail.setAddress(contact.getEmail());
        primaryMail.setRel("http://schemas.google.com/g/2005#home");

        primaryMail.setPrimary(true);
        createContact.addEmailAddress(primaryMail);

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneNumber(contact.getPhone());

        return createContact;
    }

    private void sendBatch(ContactFeed contactFeed) {
        ContactFeed responseFeed =
                null;
        try {
            responseFeed = service.batch(new URL("https://www.google.com/m8/feeds/contacts/default/full/batch"),
                    contactFeed);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        }

//        for (ContactEntry entry : responseFeed.getEntries()) {
//            String batchId = BatchUtils.getBatchId(entry);
//            BatchStatus status = BatchUtils.getBatchStatus(entry);
//            System.out.println(batchId + ": " + status.getCode() + " (" + status.getReason() + ")");
//        }

    }
}
